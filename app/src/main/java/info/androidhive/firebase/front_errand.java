package info.androidhive.firebase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


public class front_errand extends AppCompatActivity {

    private EditText errand, errand_decr;
    private Button sub;
    private DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();
    private Button send, cancel;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private FirebaseDatabase firebaseInstance;
    String temp_key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.front_errand);

        errand = (EditText) findViewById(R.id.errand_name);
        errand_decr = (EditText) findViewById(R.id.description);
        sub = (Button) findViewById(R.id.submit);
        auth = FirebaseAuth.getInstance();
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    String email = user.getEmail();
                    boolean emailVerified = user.isEmailVerified();

                    String uid = user.getUid();
                    String err = errand.getText().toString();
                    String de = errand_decr.getText().toString();


                    Map<String,Object> map = new HashMap<String, Object>();
                    firebaseInstance = FirebaseDatabase.getInstance();
                    root = firebaseInstance.getReference("Errands");
                    temp_key = root.push().getKey();
                    root.updateChildren(map);

                    DatabaseReference message_root = root.child(temp_key);
                    Map<String,Object> map2 = new HashMap<String, Object>();
                    map2.put("Errand", err);
                    map2.put("Description", de);
                    map2.put("User",  email);
                    map2.put("Assigned", null);
                    map2.put("Status", null);
                    message_root.updateChildren(map2);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Login to enter an errand", Toast.LENGTH_LONG).show();
                }
                Toast.makeText(getApplicationContext(),"New errand added", Toast.LENGTH_LONG).show();
                errand.setText("");
                errand_decr.setText("");

            }

    });
}}
