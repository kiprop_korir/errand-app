package info.androidhive.firebase.model;

/**
 * Created by korir on 6/28/2017.
 */

public class Errand {


    public String eKey , eUser , eDescription , eName;


    public String geteKey() {
        return eKey;
    }

    public void seteKey(String android_version_name) {
        this.eKey = android_version_name;
    }


    public String geteUser() {
        return eUser;
    }

    public void seteUser(String android_version_name) {
        this.eUser = android_version_name;
    }

    public String geteDescription() {
        return eDescription;
    }

    public void seteDescription(String android_version_name) {
        this.eDescription = android_version_name;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String android_version_name) {
        this.eName = android_version_name;
    }


}
