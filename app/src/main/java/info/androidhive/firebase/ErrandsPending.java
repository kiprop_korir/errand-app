package info.androidhive.firebase;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import info.androidhive.firebase.model.Errand;


public class ErrandsPending extends Fragment{
    private static final String TAG = "Tab2Fragment";
    public ArrayList<Errand> errandsList = new ArrayList<Errand>();
    ListView mListView;

    private DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    private FirebaseDatabase firebaseInstance;
    String back_email;
    String errand_name , errand_description , front_user_name , assigned_user_name , key ;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1,container,false);
        mListView = (ListView)  view.findViewById(R.id.mylistview);
        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference errandsRef = rootRef.child("Errands");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            back_email = user.getEmail();

            //System.out.print("lkhgf "+back_email);
        }
            ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {

                    System.out.print("kjh " + ds.toString());
                 errand_name = ds.child("Errand").getValue(String.class);
                  errand_description = ds.child("Description").getValue(String.class);
                    assigned_user_name = ds.child("Assigned").getValue(String.class);
                    front_user_name= ds.child("User").getValue(String.class);
                    String status= ds.child("Status").getValue(String.class);


                   key = ds.getKey();

                    Errand errand = new Errand();

                    errand.seteDescription(errand_description);
                    errand.seteKey(key);
                    errand.seteName(errand_name);
                    errand.seteUser(front_user_name);

                    if(assigned_user_name.equals(back_email)) {
                        errandsList.add(errand);
                    }




/*
                    //ll.clear();
                    mListView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String item = (String) list.getItemAtPosition(position);


                            Toast.makeText(getActivity(),"You selected : " + item,Toast.LENGTH_SHORT).show();

//                            for (DataSnapshot sn : dataSnapshot.getChildren())
//                            {
//                                String n;
//                                Log.i(item, n = sn.getKey().toString());
//                                final String key = sn.child(n).getValue(String.class);
//                                Toast.makeText(getActivity(),"You selected : " + n,Toast.LENGTH_SHORT).show();
//
//
//
//                                String er = sn.child("Errand").getValue(String.class);
//                                String dc = sn.child("Description").getValue(String.class);
//
//                            }

                            Intent intent = new Intent(getActivity(), ErrandDetails.class);
//                                intent.putExtra("user_name", User);
//                                intent.putExtra("errand", Errand);
//                                intent.putExtra("description", Desc);
                            getActivity().startActivityForResult(intent, 0);
                            Toast.makeText(getActivity(),"You selected : " + item,Toast.LENGTH_SHORT).show();


                        }
                    });
                }*/
                }
                mListView.setAdapter(new ErrandsListAdapter(getActivity()));

                mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(getContext());
                        }
                        builder.setTitle("Confirm Done ?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete


                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    String back_email = user.getEmail();


                    Map<String,Object> map = new HashMap<String, Object>();
                    firebaseInstance = FirebaseDatabase.getInstance();
                    root = firebaseInstance.getReference("Errands");

                    root.updateChildren(map);

                    DatabaseReference message_root = root.child(key);
                    Map<String,Object> map2 = new HashMap<String, Object>();
                    map2.put("Errand", errand_name  );
                    map2.put("Description", errand_description);
                    map2.put("User",  front_user_name);
                    map2.put("Assigned", back_email);
                    map2.put("Status ", "1");
                    message_root.updateChildren(map2);

                    Toast.makeText(getContext()," Errand marked as done", Toast.LENGTH_LONG).show();

                }
                else
                {
                    Toast.makeText(getContext(),"Please Login to enter an errand", Toast.LENGTH_LONG).show();
                }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                        return true;
                    }
                });
               mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        TextView tv_errand_name = (TextView) view.findViewById(R
                                .id.tv_errand_name);
                        TextView tv_errand_desc = (TextView) view.findViewById(R
                                .id.tv_errand_description);
                        TextView tv_errand_user = (TextView) view.findViewById(R
                                .id.tv_errand_user);
                        TextView tv_errand_key = (TextView) view.findViewById(R
                                .id.tv_errand_key);

                        String errand_name=tv_errand_name.getText().toString();
                        String errand_desc=tv_errand_desc.getText().toString();
                        String errand_user=tv_errand_user.getText().toString();
                        String errand_key=tv_errand_key.getText().toString();


                        Toast.makeText(getActivity(),"You selected : " + errand_name,Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), ErrandDetails.class);
                               intent.putExtra("user_name", errand_user);
                              intent.putExtra("errand", errand_name);
                               intent.putExtra("description", errand_desc);
                        intent.putExtra("key", errand_key);

                     startActivity(intent);
                    }
                });
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        registerForContextMenu(mListView);
        errandsRef.addListenerForSingleValueEvent(eventListener);
        return view;
    }

    private class ErrandsListAdapter extends BaseAdapter {
        LayoutInflater inflater;
        ViewHolder viewHolder;

        public ErrandsListAdapter(Context context) {
            // TODO Auto-generated constructor stub
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return errandsList.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (view == null) {

                ;
                view = inflater.inflate(R.layout.errands_list_item, null);
                viewHolder = new ViewHolder();

                viewHolder.tv_key = (TextView) view.findViewById(R
                        .id.tv_errand_key);
                viewHolder.tv_user = (TextView) view.findViewById(R
                        .id.tv_errand_user);
                viewHolder.tv_name = (TextView) view.findViewById(R
                        .id.tv_errand_name);
                viewHolder.tv_description = (TextView) view.findViewById(R
                        .id.tv_errand_description);


                view.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            viewHolder.tv_name.setText(errandsList.get(position).geteName());
            viewHolder.tv_key.setText(errandsList.get(position).geteKey());
            viewHolder.tv_description.setText(errandsList.get(position).geteDescription());
            viewHolder.tv_user.setText(errandsList.get(position).geteUser());






            return view;

        }


        private class ViewHolder {
            private TextView tv_name, tv_description, tv_user, tv_key;


        }


    }
    }