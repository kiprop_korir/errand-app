package info.androidhive.firebase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import info.androidhive.firebase.model.Errand;

/**
 * Created by korir on 6/28/2017.
 */

public class NewErrands extends AppCompatActivity {

   public ArrayList<Errand> errandsList = new ArrayList<Errand>();
    ListView mListView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_errands);

        mListView = (ListView) findViewById(R.id.mylistview);
        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference errandsRef = rootRef.child("Errands");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {

                    System.out.print("kjh " + ds.toString());
                    final String errand_name = ds.child("Errand").getValue(String.class);
                    final String description = ds.child("Description").getValue(String.class);
                    final String user = ds.child("User").getValue(String.class);
                    final String key = ds.getKey();

                    Errand errand = new Errand();

                    errand.seteDescription(description);
                    errand.seteKey(key);
                    errand.seteName(errand_name);
                    errand.seteUser(user);




                    errandsList.add(errand);


                }
                mListView.setAdapter(new ErrandsListAdapter(NewErrands.this));
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        TextView tv_errand_name = (TextView) view.findViewById(R
                                .id.tv_errand_name);
                        TextView tv_errand_desc = (TextView) view.findViewById(R
                                .id.tv_errand_description);
                        TextView tv_errand_user = (TextView) view.findViewById(R
                                .id.tv_errand_user);
                        TextView tv_errand_key = (TextView) view.findViewById(R
                                .id.tv_errand_key);

                        String errand_name=tv_errand_name.getText().toString();
                        String errand_desc=tv_errand_desc.getText().toString();
                        String errand_user=tv_errand_user.getText().toString();
                        String errand_key=tv_errand_key.getText().toString();


                        Toast.makeText(NewErrands.this
                                ,"You selected : " + errand_name,Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(NewErrands.this, ErrandDetails.class);
                        intent.putExtra("user_name", errand_user);
                        intent.putExtra("errand", errand_name);
                        intent.putExtra("description", errand_desc);
                        intent.putExtra("key", errand_key);

                        startActivity(intent);
                    }
                });
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        registerForContextMenu(mListView);
        errandsRef.addListenerForSingleValueEvent(eventListener);

    }
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, view,menuInfo);
        MenuInflater i = NewErrands.this.getMenuInflater();
        i.inflate(R.menu.context_menu, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.display_details:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private class ErrandsListAdapter extends BaseAdapter {
        LayoutInflater inflater;
        ErrandsListAdapter.ViewHolder viewHolder;

        public ErrandsListAdapter(Context context) {
            // TODO Auto-generated constructor stub
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return errandsList.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (view == null) {

                ;
                view = inflater.inflate(R.layout.errands_list_item, null);
                viewHolder = new ErrandsListAdapter.ViewHolder();

                viewHolder.tv_key = (TextView) view.findViewById(R
                        .id.tv_errand_key);
                viewHolder.tv_user = (TextView) view.findViewById(R
                        .id.tv_errand_user);
                viewHolder.tv_name = (TextView) view.findViewById(R
                        .id.tv_errand_name);
                viewHolder.tv_description = (TextView) view.findViewById(R
                        .id.tv_errand_description);


                view.setTag(viewHolder);

            } else {
                viewHolder = (ErrandsListAdapter.ViewHolder) view.getTag();
            }

            viewHolder.tv_name.setText(errandsList.get(position).geteName());
            viewHolder.tv_key.setText(errandsList.get(position).geteKey());
            viewHolder.tv_description.setText(errandsList.get(position).geteDescription());
            viewHolder.tv_user.setText(errandsList.get(position).geteUser());






            return view;

        }


        private class ViewHolder {
            private TextView tv_name, tv_description, tv_user, tv_key;


        }



    }
}
