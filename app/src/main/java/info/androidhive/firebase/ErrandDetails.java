package info.androidhive.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by linda on 24/06/17.
 */

public class ErrandDetails extends AppCompatActivity {
String errand_name , errand_description , front_user_name , assigned_user_name , key ;
    TextView tv_errand , tv_description , tv_user_name;
    Button btn_accept_errand;



    private DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    private FirebaseDatabase firebaseInstance;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.errand_details);
        setTitle("New Errands");

        errand_name=getIntent().getStringExtra("errand");
        errand_description=getIntent().getStringExtra("description");
        front_user_name=getIntent().getStringExtra("user_name");
        key=getIntent().getStringExtra("key");

        tv_description=(TextView)findViewById(R.id.description);
        tv_user_name=(TextView)findViewById(R.id.user);
        tv_errand=(TextView)findViewById(R.id.errand);


        tv_description.setText(errand_description);
        tv_errand.setText(errand_name);
        tv_user_name.setText(front_user_name);

        btn_accept_errand=(Button)findViewById(R.id.btn_accept_errand);

        btn_accept_errand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    String back_email = user.getEmail();


                    Map<String,Object> map = new HashMap<String, Object>();
                    firebaseInstance = FirebaseDatabase.getInstance();
                    root = firebaseInstance.getReference("Errands");

                    root.updateChildren(map);

                    DatabaseReference message_root = root.child(key);
                    Map<String,Object> map2 = new HashMap<String, Object>();
                    map2.put("Errand", errand_name  );
                    map2.put("Description", errand_description);
                    map2.put("User",  front_user_name);
                    map2.put("Assigned", back_email);
                    map2.put("Status ", null);
                    message_root.updateChildren(map2);

                    Toast.makeText(getApplicationContext()," email  :"+back_email +"  "+key, Toast.LENGTH_LONG).show();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Login to enter an errand", Toast.LENGTH_LONG).show();
                }

            }

        });
            }

}
