package info.androidhive.firebase;

/**
 * Created by linda on 27/06/17.
 */

public class Users {

    String username;

    String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
