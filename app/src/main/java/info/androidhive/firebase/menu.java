package info.androidhive.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by linda on 24/06/17.
 */

public class menu extends AppCompatActivity {
    //Context mContext = this;

    Button my_err;
    Button new_err;
    Button my_det;
    Button chat;
    Button sign_out;
    Button front;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        my_err=(Button) findViewById(R.id.my_errand);
        new_err=(Button) findViewById(R.id.new_errands);
        my_det=(Button) findViewById(R.id.change_details);
        sign_out=(Button) findViewById(R.id.sign_out);
        chat = (Button) findViewById(R.id.chat_but);
        front = (Button) findViewById(R.id.front_errands);

}

public void new_errands(View v)
{
    Intent i = new Intent(this, NewErrands.class);
    startActivity(i);

}
public void my_errands(View v)
{
    Intent i = new Intent(this, MyErrands.class);
    startActivity(i);
}
public void my_details(View v)
{
    Intent i = new Intent(this, MainActivity.class);
    startActivity(i);
}
public void chat(View v)
{
    Intent i = new Intent(this, chat_users.class);
    startActivity(i);
}
public void front_e (View v)
{
    Intent i = new Intent(this, front_errand.class);
    startActivity(i);
}
}